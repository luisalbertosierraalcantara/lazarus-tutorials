program project1;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Classes,
  SysUtils;

begin

try
    try
      WriteLn('Hello Luis');
    except
      // will only be executed in case of an exception
      on E: Exception do
        ShowMessage( 'Error: '+ E.ClassName + #13#10 + E.Message );
    end;
finally
  // Always Stop by Here
end;

ReadLn;
end.

