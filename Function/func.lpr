program func;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes,SysUtils;

function Calculate(a : Integer; b : Integer) : String;
var value : String;
begin
  value := 'Result: ' + IntToStr(a * b);

  Result := value;
end;

begin
  WriteLn(Calculate(5,5));
  ReadLn;
end.
