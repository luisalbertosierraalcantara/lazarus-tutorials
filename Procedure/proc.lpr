program proc;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes,SysUtils;

procedure calculate(a : Integer; b : Integer);
var _result : String;
begin

   _result:= IntToStr(a * b);

   WriteLn('The result is: ' + _result);
   ReadLn;
end;

begin
   calculate(5,5);
end.

