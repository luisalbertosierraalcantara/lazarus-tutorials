program if_statements;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes,
  SysUtils;

var a,b : Integer;

begin
 a := 100;
 b := 20;

 if a < b then
    begin
      WriteLn('A is less than B');
      ReadLn;
    end
 else
 if a > b then
    begin
      WriteLn('A is greater than B');
      ReadLn;
    end;

end.

