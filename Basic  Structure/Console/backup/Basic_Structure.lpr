program Basic_Structure;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Classes,
  SysUtils
  { you can add units after this };

begin

  try
    WriteLn('Hello World!');
    WriteLn('Press ENTER to Exit');
    ReadLn;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;

end.

