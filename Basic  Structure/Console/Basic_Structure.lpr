program Basic_Structure;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Classes,
  SysUtils
  { you can add units after this };

begin

    WriteLn('Hello World!');
    WriteLn('Press ENTER to Exit');
    ReadLn;

end.

