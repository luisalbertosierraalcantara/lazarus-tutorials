program Data_Type_Conversion;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Classes,SysUtils;

var _date : TDate;
    number : Integer;
    decimal : Double;
    boleano : Boolean;
    text : String;
    _time : TTime;
    date_time : TDateTime;
    ascii : byte;
    dayName : String;
    dayNumber : Integer;


begin
    _date:= Date;
    number:= 1;
    decimal:= 2.6;
    Boleano := False;
    text:= 'Hello';

    WriteLn('Convert to String');
    WriteLn('----------------------------');
    text := DateToStr(_date);
    WriteLn('String Date: '+text);
    text := IntToStr(number);
    WriteLn('String Number: '+text);
    text :=FloatToStr(decimal);
    WriteLn('String Decimal: '+text);
    text :=BoolToStr(Boleano,true);
    WriteLn('String Boleano: '+text);
    WriteLn(Chr(13));

    WriteLn('Convert to int');
    WriteLn('----------------------------');
    text := '15';
    number := StrToInt(text);
    WriteLn('Int Number: '+IntToStr(number));
    WriteLn(Chr(13));

    WriteLn('Convert to Double');
    WriteLn('----------------------------');
    text := '19.5';
    decimal:= StrToFloat(text);
    WriteLn('Double Decimal: '+FloatToStr(decimal));
    WriteLn(Chr(13));

    WriteLn('Convert to Boolean');
    WriteLn('----------------------------');
    text := 'True';
    Boleano:= StrToBool(text);
    WriteLn('Boolean Boleano: '+BoolToStr(Boleano,true));
    WriteLn(Chr(13));

    WriteLn('Convert to Date');
    WriteLn('----------------------------');
    text := '13/10/2022';
    _date := StrToDate(text);
    WriteLn('Date: '+DateToStr(_date));
    WriteLn(Chr(13));

    WriteLn('Convert to Time');
    WriteLn('----------------------------');
    text := '12:00';
    _time := StrToTime(text);
    WriteLn('Time: '+TimeToStr(_time));
    WriteLn(Chr(13));

    WriteLn('Convert to DateTime');
    WriteLn('----------------------------');
    text := '13/10/2022 12:00';
    date_time := StrToDateTime(text);
    WriteLn('DateTime: '+DateTimeToStr(date_time));
    WriteLn(Chr(13));

    WriteLn('Format DateTime');
    WriteLn('----------------------------');
    text := '13/10/2022 12:00';
    date_time := StrToDateTime(text);
    WriteLn('Format DateTime: '+FormatDateTime('dd mmmm yyyy',date_time));
    WriteLn(Chr(13));

    WriteLn('DayName of DateTime');
    WriteLn('----------------------------');
    text := '13/10/2022 12:00';
    date_time := StrToDateTime(text);
    dayNumber := DayOfWeek(date_time);

    if dayNumber = 1 then
       dayName:= 'Sunday'
    else
    if dayNumber = 2 then
       dayName:= 'Monday'
    else
    if dayNumber = 3 then
       dayName:= 'Tuesday'
    else
    if dayNumber = 4 then
       dayName:= 'Wednesday'
    else
    if dayNumber = 5 then
       dayName:= 'Thursday'
    else
    if dayNumber = 6 then
       dayName:= 'Friday'
    else
    if dayNumber = 7 then
       dayName:= 'Saturday';

    WriteLn('Day : '+ dayName);
    WriteLn(Chr(13));


    WriteLn('Convert to ASCII');
    WriteLn('----------------------------');
    WriteLn('ASCII: '+ Chr(65));
    WriteLn(Chr(13));

    WriteLn('String Replace');
    WriteLn('----------------------------');
    text:= 'Pascal';
    WriteLn(text);
    text := text.Replace('s','X');
    WriteLn(text);
    WriteLn(Chr(13));

    WriteLn('String Length');
    WriteLn('----------------------------');
    text:= 'Pascal';
    WriteLn(text);
    WriteLn('Length: ' + text.Length.ToString);
    WriteLn(Chr(13));

    WriteLn('String Copy');
    WriteLn('----------------------------');
    text:= 'Pascal';
    WriteLn(text);
    WriteLn('Copy: ' + Copy(text,0,3));
    WriteLn(Chr(13));

    ReadLn;

end.

