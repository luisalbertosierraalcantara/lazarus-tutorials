program User_Def_Data_Types;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Classes,
  SysUtils
  { you can add units after this };

type
  //subrange definition
  NAME = 'A'..'Z';

  //definition of the matrix
  CAR = array[1..24] of Integer;

  //definition of record
  _Date = record
  _Month : string;
  _Day : Byte;
  _Year : Integer;
  end;

  //Enumerated type definition
  COLORS = (Blue,White, Black);

  var Series : CAR;
      birthday : _date;
      paint : COLORS;

  //no specific type names
  Temperature : array[1..31] of Byte;

begin

    WriteLn('print subrange');
    WriteLn(NAME(65));
    WriteLn(Chr(13)); //ASCII line break

    Series[1] := 1032;
    Series[2] := 1000;

    WriteLn('print matrix ');
    WriteLn(Series[1].ToString);
    WriteLn(Chr(13)); //ASCII line break

    birthday._Month := 'January';
    birthday._Day   := 1;
    birthday._Year  := 1995;

    WriteLn('print record');
    WriteLn(birthday._Month);
    WriteLn(Chr(13)); //ASCII line break

    Temperature[1] := 30;

    WriteLn('print type without name');
    WriteLn(Temperature[1].ToString);
    WriteLn(Chr(13)); //ASCII line break

    ReadLn;

end.


