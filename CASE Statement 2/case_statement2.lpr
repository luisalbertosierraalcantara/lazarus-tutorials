program case_statement2;

{$mode objfpc}{$H+}


uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes,
  SysUtils;

var a : Char;
begin

  ReadLn(a);

  Case a of
  'a' :
  begin
     WriteLn('Entered the letter: '+ a);
     ReadLn;
  end;
  'b' :
  begin
     WriteLn('Entered the letter: '+ a);
     ReadLn;
  end;
  else
  begin
     WriteLn('You entered a letter other than A & B: '+ a);
     ReadLn;
  end;

  end;
end.


