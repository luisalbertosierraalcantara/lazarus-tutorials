program WhileStatement;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes,
  SysUtils;

var mag,num : Integer;
begin

  num := 435679;
  mag := 0;

  WriteLn('Number: ' + num.ToString);

   while (num > 0) do
     begin
       Inc(mag);
       num := num div 10;
     end;

   WriteLn('Magnitude: ' + mag.ToString);
   ReadLn;
end.


