program for_statement;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes,SysUtils;

var a : Integer;
begin

  for a:= 1 to 15  do
   begin
     WriteLn(a.ToString);
   end;

  ReadLn;
end.


