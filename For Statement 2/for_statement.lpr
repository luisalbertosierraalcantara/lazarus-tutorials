program for_statement;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes,SysUtils;

var a : Integer;
begin

  for a:= 15 downto 1  do
   begin
     WriteLn(a.ToString);
   end;

  ReadLn;
end.


