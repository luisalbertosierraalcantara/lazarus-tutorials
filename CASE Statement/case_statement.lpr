program case_statement;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes,
  SysUtils;

var number : Integer;
begin

  number := 4;

  Case number of
  1 :
  begin
     WriteLn('One');
     ReadLn;
  end;
  2 :
  begin
     WriteLn('Two');
     ReadLn;
  end;
  else
  begin
     WriteLn('Different from 1 and 2');
     ReadLn;
  end;

  end;
end.
